import React from 'react'

class AboutMe extends React.Component {
    render() {
        return (
            <div>
                <div className="container" id="information">
                    <div className="row" id="logo1">
                        <div className="col">
                            <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                                height="100"></img>
                        </div>
                    </div>
                    <div className="row" id="rowbody">
                        <div className="col">
                            <h1 className="headerText">About</h1>
                            <hr></hr>
                        </div>
                    </div>
                    <div className="row">

                        <div className="col">
                            <div className="row">
                                <img src={require('../srisoonthorn165/profile_srisoonthorn.jpg')} className="w3-circle" alt="Norway"></img>
                            </div>
                            <div className="row-12">
                                <div className="col-6">
                                    <h1 className="headerText">Srisoonthorn Nongpingkham</h1>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <h2 class="text-light bg-white"><i className='far fa-address-card'></i> Information</h2>
                            <div className="row-5">
                                <div className="col-12">

                                    <p><b>Nickname</b> : Game</p>
                                    <p><b>Birthday</b> : 23/01/1999</p>
                                    <p><b>Age</b> : 21</p>
                                </div>
                            </div>
                            <hr></hr>
                            <h2 className="text-light bg-white"><i class='far fa-envelope'></i> Contact </h2>
                            <div className="row-5">
                                <div className="col-12">
                                    <p><b>Address</b> :Hang Chat Lampang 52190</p>
                                    <p><b>Email</b> : srisoonthorn_n@cmu.ac.th</p>
                                    <p><b>Phone</b> : 0842215170</p>
                                </div>
                            </div>
                            <hr></hr>
                            <h2 className="text-light bg-white"><i class='fas fa-graduation-cap'></i> Education </h2>
                            <div className="row-5">
                                <div className="col-12">
                                    <p><b>Hight School</b> : Mattayom Wittaya School Lampang</p>
                                    <p><b>University</b> : Chiangmai University</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        )

    }
}
export default AboutMe