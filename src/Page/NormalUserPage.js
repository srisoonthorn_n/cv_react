import React from 'react'

class NormalUserPage extends React.Component {

    constructor() {
        super();
        this.state = {
            users: []
        }
    }

    componentDidMount() {
        this.fetchUsers();
    }

    fetchUsers() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {

                this.setState({ users: data })

            })
            .catch(error => console.log(error));

    }

    render() {

        const { users } = this.state;

        return (
            <div className="row">
                <div>
                    {
                        users.map((item, index) => {
                            return (
                                <div key={index}>
                                    Hello
                            <p>Id : {item.id}</p>
                                    <p>Name : {item.name}</p>
                                    <p>UserName : {item.username}</p>
                                    <p>Email : {item.email}</p>
                                    <hr />
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }

}

export default NormalUserPage;