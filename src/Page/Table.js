import React, { useState, useEffect } from 'react'
import { dataAll } from '../component/data'
import '../cv_index.css';

const StudentReportNew = () => {

    const [semsters, setSemsters] = useState([]);
    const [indexing, setIndex] = useState([-1]);


    useEffect(() => {
        fetchUser();
    }, [])

    const fetchUser = () => {
        setSemsters(dataAll);
    }

    const onClickfunction = (event) => {
        let i = event.target.value
        setIndex(i)

    }

    return (

        < div >
            <div className="container" id="information">
                <div className="row" id="logo1">
                    <div className="col">
                        <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                            height="100"></img>
                    </div>
                </div>

                <div className="row" id="rowbody">
                    <div className="col">
                        <h1 className="headerText">Student Report</h1>
                        <h3><a href="/processLogout">Logout</a></h3>
                        <div className="row">
                            <div className="col">
                                <h5 className="headerText">Student ID : 602110165</h5>
                                <h5 className="headerText">University: Chiang Mai University</h5>
                                <h5 className="headerText">Faculty : College of Arts, Media and Technology</h5>
                                <h5 className="headerText">Major : Modern Management Information And Technology</h5>
                                <hr className="my-4"></hr>
                            </div>
                        </div>
                        <center>
                            <div className="col-md-5 ">
                                <select class="form-control" required onChange={onClickfunction}>
                                    <option disabled >Choose Term...</option>
                                    <option selected value={-1}>All Term</option>
                                    {
                                        semsters.map((char, index) => {
                                            return (
                                                <option value={index}>{char.year}</option>
                                            )
                                        })


                                    }
                                </select>
                            </div>
                        </center>
                        <hr></hr>
                    </div>

                </div>

                {
                    semsters.map(({ year, enrolledsubjects, gpas }, index) => {
                        if (index == indexing || indexing == -1) {

                            return (

                                <div key={index}>

                                    <div className="row-5">

                                        <h1 className="headerText">Semester{year}</h1>
                                        <table className="table table-hover table-bordered " id="table1">
                                            <thead>
                                                <tr>
                                                    <th>Course Id</th>
                                                    <th>Course Name</th>
                                                    <th>Credit</th>
                                                    <th>Grade</th>
                                                </tr>
                                            </thead>
                                            {enrolledsubjects.map((subject, index) => {
                                                return (
                                                    <tbody>
                                                        <tr>
                                                            <td>{subject.subjectId}</td>
                                                            <td>{subject.subjectName}</td>
                                                            <td>{subject.credit}</td>
                                                            <td>{subject.grade}</td>
                                                        </tr>
                                                    </tbody>
                                                )

                                            })}

                                        </table>
                                        <table className="table table-bordered">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col">Record</th>
                                                    <th scope="col">CA</th>
                                                    <th scope="col">CE</th>
                                                    <th scope="col">GPA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Semester {year}</td>
                                                    <td>{gpas.currentSem.enrolledCredits}</td>
                                                    <td>{gpas.currentSem.recievedCredits}</td>
                                                    <td>{gpas.currentSem.currentGpa}</td>
                                                </tr>
                                                <tr>
                                                    <th>Cumulative</th>
                                                    <td>{gpas.cumulative.enrolledCredits}</td>
                                                    <td>{gpas.cumulative.recievedCredits}</td>
                                                    <td>{gpas.cumulative.cumulativeGpa}</td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            )
                        }
                    })
                }

            </div>

        </div >

    )
}

export default StudentReportNew