import React from 'react'

class StudentReport extends React.Component {
    render() {
        return (
            <div>


                <div className="container" id="information">
                    <div className="row" id="logo1">
                        <div className="col">
                            <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                                height="100"></img>
                        </div>
                    </div>
                    <div className="row" id="rowbody">
                        <div className="col">
                            <h1 className="headerText">Student Report</h1>


                            <h3><a href="/processLogout">Logout</a></h3>
                            <hr></hr>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <h5 className="headerText">Student ID : 602110165</h5>
                            <h5 className="headerText">University: Chiang Mai University</h5>
                            <h5 className="headerText">Faculty : College of Arts, Media and Technology</h5>
                            <h5 className="headerText">Major : Modern Management Information And Technology</h5>
                            <hr className="my-4"></hr>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col">
                            <h3 className="headerText">Semester : 1/2560</h3>
                            <table className="table table-hover table-bordered " id="table1">
                                <thead className="thead-light">
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Course id</th>
                                        <th scope="col">Course title</th>
                                        <th scope="col">Credit</th>
                                        <th scope="col">Grade</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>001101</td>
                                        <td>FUNDAMENTAL ENGLISH 1</td>
                                        <td>3</td>
                                        <td>C+</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>109100</td>
                                        <td>MAN AND ART</td>
                                        <td>3</td>
                                        <td>B</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>206171</td>
                                        <td>GENERAL MATHEMATICS 1</td>
                                        <td>3</td>
                                        <td>C</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>851100</td>
                                        <td>INTRO TO COMMUNICATION</td>
                                        <td>3</td>
                                        <td>C+</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">5</th>
                                        <td>953111</td>
                                        <td>SOFTWARE FOR EVERYDAY LIFE</td>
                                        <td>3</td>
                                        <td>B</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">6</th>
                                        <td>954100</td>
                                        <td>INFO SYSTEM FOR ORG MGMT</td>
                                        <td>3</td>
                                        <td>C</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">7</th>
                                        <td>954140</td>
                                        <td>IT LITERACY</td>
                                        <td>3</td>
                                        <td>B+</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div className="table-block table-responsive-sm">
                                <table className="table table-bordered">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">Record</th>
                                            <th scope="col">CA</th>
                                            <th scope="col">CE</th>
                                            <th scope="col">GPA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Semester 1 / 2560</td>
                                            <td>21.00</td>
                                            <td>21.00</td>
                                            <td>2.64</td>
                                        </tr>
                                        <tr>
                                            <th>Cumulative</th>
                                            <td>21.00</td>
                                            <td>21.00</td>
                                            <td>2.64</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        )

    }
}
export default StudentReport