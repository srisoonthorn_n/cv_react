import React from 'react';

class LoginPage extends React.Component {

    constructor() {
        super()
        this.state = {

        }
    }

    render() {
        return (
            <div>
                <div className="container" id="information">
                    <div className="row" id="logo1">
                        <div className="col">
                            <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                                height="100"></img>
                        </div>
                    </div>
                    <div className="row" id="rowbody">
                        <div className="col">
                            <h2 className="headerText">Login</h2>
                            <hr></hr>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <h3 className="headerText">UserName</h3>
                            <input type="text" placeholder="name@example.com"></input>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <h3 className="headerText">Password</h3>
                            <input type="text" placeholder="**********"></input>
                        </div>
                    </div>

                    <div className="row">
                        <hr></hr>
                    </div>
                    <div className="row">
                        <div className="col">
                            <button className="btn btn-primary btn-lg" onClick={() => window.location = "/processLogin"}>Login</button>
                        </div>
                    </div>
                    <hr></hr>
                </div>
            </div >


        )
    }
}

export default LoginPage