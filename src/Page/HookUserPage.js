import React, { useState } from 'react'

const HookUserPage = () => {

    const [users, setUsers] = useState([]);
    const [grade, setGrade] = useState('F');

    useEffect(() => {
        fetchUsers();
        fetchGrade();
    }, [])

    const fetchUsers = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {

                setUsers(data);

            })
            .catch(error => console.log(error));

    }

    const fetchGrade = () => {
        setGrade('A');
    }

    return (
        <div className="row">
            <div>
                {
                    users.map((item, index) => {
                        return (
                            <div key={index}>
                                Hello
                                <p>Id : {item.id}</p>
                                <p>Name : {item.name}</p>
                                <p>UserName : {item.username}</p>
                                <p>Email : {item.email}</p>
                                <hr />
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )

}

export default HookUserPage;