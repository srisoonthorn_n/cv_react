import React from 'react'

class Speciality extends React.Component {
    render() {
        return (
            <div>
                <div className="container" id="information">
                    <div className="row" id="logo1">
                        <div className="col">
                            <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                                height="100"></img>
                        </div>
                    </div>
                    <div className="row" id="rowbody">
                        <div className="col">
                            <h1 className="headerText">Speciality</h1>
                            <hr></hr>
                        </div>
                    </div>
                    <div className="row" >
                        <div className="col">
                            <div className="d-flex bd-highlight">
                                <div className="p-2 flex-fill bd-highlight">
                                    <img src={require('../srisoonthorn165/english-language.png')} className="img-thumbnail" width="250"
                                        height="100"></img><h2 className="headerText">English Skill : Basic</h2></div>
                                <div className="p-2 flex-fill bd-highlight"></div>
                                <div className="p-2 flex-fill bd-highlight">
                                    <img src={require('../srisoonthorn165/swimming.png')} className="img-thumbnail" width="250"
                                        height="100"></img><h2 className="headerText">Swimming Skill : Basic</h2></div>
                            </div>

                            <div className="row">
                                <div className="col">
                                    <div className="d-flex bd-highlight">
                                        <div className="p-2 flex-fill bd-highlight">
                                            <img src={require('../srisoonthorn165/html.png')} className="img-thumbnail" width="250"
                                                height="100"></img><h2 className="headerText">HTML : Basic</h2></div>
                                        <div className="p-2 flex-fill bd-highlight"></div>
                                        <div className="p-2 flex-fill bd-highlight">
                                            <img src={require('../srisoonthorn165/soccer.png')} className="img-thumbnail" width="250"
                                                height="100"></img><h2 className="headerText">Football : Basic</h2></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div >
        )

    }
}

{/* <div className="container" id="information" >
<div className="row" id="logo1">
    <div className="col">
        <img src="" className="img-fluid" alt="Responsive image"></img>
    </div>
</div>
<div className="row" id="rowbody">
    <div className="col">
        <h2>Speciality</h2>
        <hr className="my-4"></hr>
    </div>
</div>

<div className="row" >
    <div className="col">
        <div className="d-flex bd-highlight">
            <div className="p-2 flex-fill bd-highlight"><img src="" className="img-thumbnail"><h2>English Skill : Basic</h2></img></div>
            <div className="p-2 flex-fill bd-highlight"></div>
            <div className="p-2 flex-fill bd-highlight"><img src="" className="img-thumbnail"><h2>Swimming Skill : Basic</h2></img></div>
        </div>

        <div className="row">
            <div className="col">
                <div className="d-flex bd-highlight">
                    <div className="p-2 flex-fill bd-highlight"><img src="" className="img-thumbnail"><h2>HTML : Basic</h2></img></div>
                    <div className="p-2 flex-fill bd-highlight"></div>
                    <div className="p-2 flex-fill bd-highlight"><img src="" className="img-thumbnail"><h2>Football : Basic</h2></img></div>
                </div>
            </div>
        </div>
    </div>
</div>

</div> */}
export default Speciality