import React, { useState, useEffect } from 'react'

const UserPageFunc = () => {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetchUser();
    }, [])



    const fetchUser = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                // console.log(data);
                // this.setState({ users: data })
                setUsers(data)
            })
            .catch(error => console.log(error));

    }
    return (
        <div>
            <div className="container" id="information">
                <div className="row" id="logo1">
                    <div className="col">
                        <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                            height="100"></img>
                    </div>
                </div>
                <div className="row" id="rowbody">
                    <div className="col">
                        <div><h1>User Page Fucn</h1></div>
                        <hr></hr>
                    </div>
                </div>
                <div className="row">
                    <div>
                        {
                            users.map((item, index) => {
                                return (
                                    <div key={index}>
                                        Hello
                            <p>Id : {item.id}</p>
                                        <p>Name : {item.name}</p>
                                        <p>UserName : {item.username}</p>
                                        <p>Email : {item.email}</p>
                                        <hr />
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>

    );
}

export default UserPageFunc