import React, { useState, useEffect } from 'react'
import { Card, Col, Row, List, Avatar, Input, Icon, Select } from 'antd';

const Anime = () => {
    const { Search } = Input;
    const { Meta } = Card;
    const [nameAnime, setNameAnime] = useState([]);
    const [name, setName] = useState('naruto&limit=15')
    const [isLoading, setIsLoading] = useState(false)

    const [selectType, SetSelectType] = useState([]);
    const [selectedTypeIndex, SetSelectedTypeIndex] = useState(-1)
    const { Option } = Select;


    useEffect(() => {
        onClickfunction();
    }, [name])

    const onClickfunction = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=' + name)
            .then(response => response.json())
            .then(data => {
                //set data
                setIsLoading(false)
                setNameAnime(data.results)
                GetType(data.results)
            })
            .catch(error => console.log(error));
    }
    const setStateName = (value) => {
        setIsLoading(true)
        setName(value)
    }
    const IconText = ({ type, text }) => (
        <span>
            <Icon type={type} style={{ marginRight: 8 }} />
            {text}
        </span>
    );

    const GetType = (nameAnimes) => {
        let getType = [];

        nameAnimes.map((item) => {
            if (!getType.includes(item.type)) {
                getType.push(item.type)
            }
        });
        SetSelectType(getType)
    }

    const onFilterType = (index) => {
        let keepType = index
        SetSelectedTypeIndex(keepType)
    }

    console.log(selectType)
    return (

        < div >
            <div className="container" id="information">
                <div className="row" id="logo1">
                    <div className="col">
                        <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                            height="100"></img>
                    </div>
                </div>
                <div className="row" id="rowbody">
                    <div className="col">
                        <div><h1 className="headerText">Anime</h1></div>
                        <div>
                            <Row>
                                <Col>
                                    <Search
                                        placeholder="input search Anime"
                                        style={{ width: 400 }}
                                        enterButton="Search"
                                        onSearch={setStateName}
                                    />
                                    <Select defaultValue={-1} style={{ width: 120, float: 'right' }} onChange={onFilterType}>
                                        <Option value={-1} >All Tpye</Option>
                                        {
                                            selectType.map((itemType, index) => {
                                                return (
                                                    <Option value={index} >{itemType}</Option>
                                                )
                                            })
                                        }
                                    </Select>
                                </Col>
                            </Row>
                            <hr></hr>
                        </div>
                    </div>
                </div>
                <List
                    itemLayout="horizontal"
                    loading={isLoading}
                    dataSource={nameAnime}
                    itemLayout="vertical"
                    size="large"
                    pagination={
                        {
                            onChange: page => {
                                console.log(page);
                            },
                            pageSize: 5,
                        }}
                    dataSource={
                        selectedTypeIndex == -1 ?
                            nameAnime
                            :
                            nameAnime.filter(m => m.type == selectType[selectedTypeIndex])
                    }
                    footer={
                        <div>

                        </div>
                    }
                    renderItem={itemA => (
                        <Row>
                            <Col span={24}>
                                <Card actions={[
                                    <IconText type="star-o" text={itemA.score} key="list-vertical-star-o" />,
                                    <IconText type="like-o" text={itemA.members} key="list-vertical-like-o" />,
                                    <IconText type="project" text={itemA.type} key="list-vertical-message" />,
                                ]}>
                                    <List.Item.Meta
                                        avatar={<img src={itemA.image_url} style={{ width: 250 }} />}
                                        title={<a href={itemA.url}><h2>{itemA.title}</h2></a>}
                                        description={itemA.synopsis}
                                    />
                                </Card>
                            </Col>
                        </Row>
                    )}
                />
                <br />
            </div>
        </div >
    )
}

export default Anime

{/* {
                    nameAnime.map((item, index) => {
                        return (
                            < div style={{ background: '#ECECEC', padding: '30px' }}>
                                <center>
                                    <Row gutter={16}>
                                        <Col span={24}>
                                            <Card title={item.title} bordered={false} cover={<img src={item.image_url} />} style={{ width: 300 }}>
                                                {item.synopsis}</Card>
                                        </Col>
                                    </Row>

                                </center>
                            </div>
                        )
                    })
                } */}