import React, { useState, useEffect } from 'react';
import { Select } from 'antd';

const { Option } = Select;

const UserPage = () => {

    const [users, setUsers] = useState([]);
    const [indexing, setIndex] = useState([-1]);

    const fetchUser = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                //set data
                setUsers(data)
            })
            .catch(error => console.log(error));




    }
    useEffect(() => {
        fetchUser();
    }, [])


    const onClickfunction = (indexing) => {
        setIndex(indexing)

    }
    return (
        <div>
            <div className="container" id="information">
                <div className="row" id="logo1">
                    <div className="col">
                        <img src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.jpg" className="img-fluid" alt="Responsive image" width="300"
                            height="100"></img>
                    </div>
                </div>
                <div className="row" id="rowbody">
                    <div className="col">
                        <div><h1>User Page </h1></div>
                        <Select defaultValue="Choose User ." style={{ width: 120 }} onChange={onClickfunction}>
                            <Option value={-1}>Choose All Name</Option>
                            {
                                users.map((user, index) => {
                                    return (
                                        <Option key={index} value={index}>{user.name}</Option>
                                    )
                                })

                            }
                        </Select>
                        <hr></hr>
                    </div>
                </div>
                <div className="row">
                    <div>
                        {
                            users.map((item, index) => {
                                if (index == indexing || indexing == -1) {


                                    return (
                                        <div key={index}>
                                            <p>Id : {item.id}</p>
                                            <p>Name : {item.name}</p>
                                            <p>UserName : {item.username}</p>
                                            <p>Email : {item.email}</p>
                                            <hr />
                                        </div>
                                    )
                                }
                            })
                        }

                    </div>
                </div>
            </div>
        </div>

    );

}
export default UserPage