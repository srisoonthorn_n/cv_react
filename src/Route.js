import React from 'react'
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Header from './component/Header';
import AboutMe from './Page/AboutMe';
import Speciality from './Page/Speciality';
import StudentReport from './Page/StudentReport';
import LoginPage from './Page/LoginPage';
import './cv_index.css';
import PrivateRoute from './component/PrivateRoute';
// import UserPageFunc from './Page/UserPageFunc';
import StudentReportNew from './Page/Table';
import UserPage from './Page/UserPage';
import Anime from './Page/Anime';

const MainRouting = () => {
    return (
        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                localStorage.setItem("isLogin", true);
                return <Redirect to="/studentreport" />
            }}></Route>

            <Route path="/processLogout" render={() => {
                localStorage.setItem("isLogin", false);
                return <Redirect to="/" />
            }}></Route>

            {/* <Route path="/" render={() => {
                return (
                    <Redirect to="/home" />
                )
            }} exact={true}>></Route> */}

            <Route path="/" component={Header} ></Route>
            <Route path="/" component={AboutMe} exact></Route>
            <Route path="/speciality" component={Speciality} ></Route>
            {/* <PrivateRoute path="/studentreport" component={StudentReport} title={2}></PrivateRoute> */}
            <Route path="/login" component={LoginPage} ></Route>
            {/* <Route path="/UserPageFunc" component={UserPageFunc}></Route> */}
            <PrivateRoute path="/studentreport" component={StudentReportNew}></PrivateRoute>
            <Route path="/UserPage" component={UserPage}></Route>
            <Route path="/Anime" component={Anime}></Route>
        </BrowserRouter>

    )
}

export default MainRouting

