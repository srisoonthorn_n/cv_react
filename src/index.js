import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import './cv_index.css';
import MainRouting from './Route';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'


//How to use LocalStorage ??

//Setter  
// localStorage.setItem("a", "Hello world");

//Getter แสดงตัวแปรมารับ

// const a = localStorage.getItem("a");
// alert(a)


ReactDOM.render(<MainRouting />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
