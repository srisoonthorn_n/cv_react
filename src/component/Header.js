import React from 'react'

class Header extends React.Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
                    <div className="navbar-brand js-scroll-trigger"></div>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse " id="navbarSupportedContent">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger " href="/"><span className="far fa-user-circle"></span>
                                    About Me
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/speciality"><span className="far fa-user-circle"></span>
                                    Speciality
                                </a>
                            </li>
                            {/* <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/studentreport"><span className="far fa-user-circle"></span>
                                    Student Report
                                </a>
                            </li> */}
                            {/* <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/UserPageFunc"><span className="far fa-user-circle"></span>
                                    Test Table
                                </a>
                            </li> */}
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/studentreport"><span className="far fa-user-circle"></span>
                                    Student Report
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/Anime"><span className="far fa-user-circle"></span>
                                    Anime
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Header