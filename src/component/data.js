export const dataAll = [
    {
        year: '1 / 2560',
        enrolledsubjects: [
            {
                subjectId: '001101',
                subjectName: "FUNDAMENTAL ENGLISH 1",
                credit: 3,
                grade: 'C+'
            },
            {
                subjectId: '109100',
                subjectName: "MAN AND ART",
                credit: 3,
                grade: 'B'
            },
            {
                subjectId: '206171',
                subjectName: "GENERAL MATHEMATICS 1",
                credit: 3,
                grade: 'C'
            },
            {
                subjectId: '851100',
                subjectName: "INTRO TO COMMUNICATION",
                credit: 3,
                grade: 'C+'
            },
            {
                subjectId: '953111',
                subjectName: "SOFTWARE FOR EVERYDAY LIFE",
                credit: 3,
                grade: 'B'
            },
            {
                subjectId: '954100',
                subjectName: "INFO SYSTEM FOR ORG MGMT",
                credit: 3,
                grade: 'C'
            },
            {
                subjectId: '954140',
                subjectName: "IT LITERACY",
                credit: 3,
                grade: 'B+'
            }
        ],
        gpas: {
            currentSem: {
                enrolledCredits: 21,
                recievedCredits: 21,
                currentGpa: 2.64
            },
            cumulative: {
                enrolledCredits: 21,
                recievedCredits: 21,
                cumulativeGpa: 2.64
            }
        }
    },
    {
        year: '2 / 2560',
        enrolledsubjects: [
            {
                subjectId: '001102',
                subjectName: "FUNDAMENTAL ENGLISH 2	",
                credit: 3,
                grade: 'C'
            },
            {
                subjectId: '013110',
                subjectName: "PSYCHOLOGY AND DAILY LIFE",
                credit: 3,
                grade: 'B'
            },
            {
                subjectId: '201114',
                subjectName: "ENVIRON SCI TODAY WORLD",
                credit: 3,
                grade: 'W'
            },
            {
                subjectId: '954141',
                subjectName: "INFORM AND COMM TECH",
                credit: 3,
                grade: 'B+'
            },
            {
                subjectId: '954142',
                subjectName: "FUNDA COM PROGRAM FOR MM",
                credit: 3,
                grade: 'C+'
            },
            {
                subjectId: '955100',
                subjectName: "LEARNING THROUGH ACTIVITIES 1",
                credit: 1,
                grade: 'A'
            }
        ],
        gpas: {
            currentSem: {
                enrolledCredits: 16,
                recievedCredits: 13,
                currentGpa: 2.85
            },
            cumulative: {
                enrolledCredits: 37,
                recievedCredits: 34,
                cumulativeGpa: 2.72
            }
        }
    }

]
