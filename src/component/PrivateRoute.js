import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ path, component: Component }) => {

    return (
        <Route path={path} render={(props) => {
            const isLogin = localStorage.getItem('isLogin')

            //alert(isLogin)
            if (isLogin == 'true')
                return <Component {...props} />
            else
                return <Redirect to="/login" />
        }} />
    )
}

export default PrivateRoute